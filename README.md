Role Docker
=========

Role Variables
--------------

#### Переменные пользователя:
  
      docker_ssh_user: ""
  ___  
#### Переменные проекта registry:

      docker_registry:
        url: "{{ lookup('env','CI_REGISTRY') }}"
        user: "gitlab-ci-token"
        password: "{{ lookup('env','CI_JOB_TOKEN') }}"
      docker_registry_login: false|true
  ___
#### Переменные проекта docker:

      docker_cron_prune: true|false
  ___
#### Переменные проекта docker:
  
      docker_project:
          image: ""
          name: "docker-app"
          port: "80"
  ___
#### Переменные запуска контейнера:

      docker_container_start: false|true
      docker_container:
        image: "{{ docker_project.image }}"
        port: "{{ docker_project.port }}"
  ___
#### Переменные compose:

      docker_compose_start: false|true
      docker_compose_remove_orphans: true
      docker_compose_project_path: "/opt/{{ docker_project.name }}"
      docker_compose_project_release: "main"
      docker_compose_services:
        - image: "{{ docker_project.image }}"
          name: "{{ docker_project.name }}"
          port: "{{ docker_project.port }}"
  ___

Example Playbook
----------------


    - hosts: servers
      roles:
        - role: 
          - username.rolename
          vars:
          - docker_project:
              image: "https://registry.example.com/image:latest"
              name: "docker-app"
              port: "80"
          # если надо запустить контейнер
          - docker_container_start: true
          # если надо запустить compose 
          - docker_compose_start: true
          # указываем релиз если надо
          - docker_compose_project_release: "v0.0.1"

License
-------

MIT

Author Information
------------------

vitoxaya
